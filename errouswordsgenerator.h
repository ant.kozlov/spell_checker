// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef ERROUSWORDSGENERATOR_H
#define ERROUSWORDSGENERATOR_H

#include <string>
#include <list>
#include <vector>

/*!
 * \brief The ErrousWordsGenerator class generates all possible corrections for given word and dictionary
 */
class ErrousWordsGenerator
{
private:    
public:
    struct ErrousWord
    {
        std::size_t pos;///<position in the dict
        std::size_t distance;///<num of the corrections
        std::string value;///<target word        

        bool operator < (const ErrousWord& b) const
        {
            return this->distance < b.distance;
        }

        bool operator == (const ErrousWord& b) const
        {
            return (this->pos==b.pos&&this->distance==b.distance&&this->value==b.value);
        }
    };

    using ErrousWords=std::list<ErrousWord>;
    using String=std::string;
    using Strings=std::vector<String>;

    explicit ErrousWordsGenerator(const std::string& src,const Strings& dict);
    const ErrousWords& GetResult(void) const {return const_cast<const ErrousWords&>(this->values);}    
private:
    ErrousWords values;
};

#endif // ERROUSWORDSGENERATOR_H
