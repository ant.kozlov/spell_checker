// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef ISTRINGSOURCE_H
#define ISTRINGSOURCE_H

#include <string>

/*!
 * \brief The IStringSource class - it's the interface to the source of strings (e.g. second part of the input file in our case)
 */
class IStringSource
{
private:
    IStringSource(const IStringSource&)=delete;
    IStringSource& operator = (const IStringSource&)=delete;
public:
    explicit IStringSource() {}
    virtual ~IStringSource() {}

    using String=std::string;
    static const std::size_t LINE_EOF=0;

    /*!
     * \brief GetString from resource. must be t-safe     
     * \param current line number or LINE_EOF and string value
     */
    virtual void GetString(std::size_t& lnumOut,String& strOut) const=0;
    /*!
     * \brief Load - init string source from resource
     * \param resource - file name
     * \param offset - offset to the text
     * \return true if ok
     */
    virtual bool Load(const String& resource, const String& delimiter,std::size_t offset)=0;
    /*!
     * \brief Free resources
     */
    virtual void Close(void)=0;
};

#endif // ISTRINGSOURCE_H
