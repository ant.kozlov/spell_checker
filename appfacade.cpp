// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "appfacade.h"
#include "memstringsource.h"
#include "simpleformatter.h"
#include "stringutils.h"

#include "simpledictionary.h"

#include <iostream>
#include <vector>
#include <sstream>

bool AppFacade::validateFilename(const std::string &fn) const
{
    //FIXME add strong check if needed
    return !fn.empty();
}

/*!
 * \brief AppFacade::processInputFile - it parses input file, fills the dictionary and returns an offset to the text
 * \param fn - filename
 * \param text_offset - offset to text, dictionary offset is assumed to be zero
 * \return true if ok
 */
bool AppFacade::processInputFile(const std::string &fn, std::size_t &text_offset, IDictionary& dict) const
{
    bool res(false);
    std::ifstream infile(fn);

    //simplest checking - file exist and contain delimiters
    //checking can be more complex if needed
    if(infile.is_open())
    {        
        std::size_t dictLinesCounter(0), offset(0);
        text_offset=0;

        while(1)
        {
            std::string line;
            std::getline(infile,line);
            if(infile.eof()||infile.fail()) break;
            offset+=line.size();
            offset++;//count 'new line' (hardcoded to one byte)
            dictLinesCounter++;

            if(line==this->DELIMITER)
            {
                //the first delimiter is founded
                text_offset=offset;
                break;
            }
            else dict.AddWordsFromLine(line);
        }

        res=(dictLinesCounter>0&&text_offset>0);
    }

    return res;
}

void AppFacade::processString(const std::string &s) const
{
    std::istringstream stream(s);
    std::string word;
    SimpleDictionary::Strings similars;
    std::size_t pos(0);

    while(std::getline(stream,word,' '))
    {
        std::string word_in_lower_case(StringUtils::toLowerStr(word));

        if(this->dictionary->FindExact(word_in_lower_case)) this->formatter->AddCorrectWord(word);
        else if(this->dictionary->FindSimilar(word_in_lower_case,similars))
        {
            //one or more corrections were found
            if(similars.size()==1) this->formatter->AddCorrectedWord(similars[0]);
            else this->formatter->AddNotCorrectedWords(similars);
        }
        else
        {
            //no corrections were found
            this->formatter->AddNotCorrectedWord(word);
        }

        //add spacer if it is not last word
        pos+=(word.size()+1);
        if(pos<s.size()) this->formatter->AddSpacer();
    }

    this->formatter->AddNewLine();
}

void AppFacade::createStringSource()
{
    this->text=std::make_unique<MemStringSource>();
}

void AppFacade::createFormatter(std::ostream &output)
{
    this->formatter=std::make_unique<SimpleFormatter>(output);
}

void AppFacade::createDictionary()
{
    this->dictionary=std::make_unique<SimpleDictionary>();
}

/*!
 * \brief AppFacade::Init - it checks input params for validity, creates needed objects and opens files
 * \param fname - the filename
 * \param m - the mask
 * \param output - output stream
 * \return true if ok
 */
bool AppFacade::Init(const std::string& in_fname, const std::string& out_fname)
{
    bool res(false);    

    if(!this->isInitialized)
    {        
        std::size_t text_offset(0);
        this->createDictionary();

        if(this->validateFilename(in_fname)&&
                this->validateFilename(out_fname)&&
                this->processInputFile(in_fname,text_offset,*this->dictionary))
        {
            //load text
            this->createStringSource();
            if(this->text->Load(in_fname,this->DELIMITER,text_offset))
            {
                this->output.open(out_fname);
                if(this->output.is_open())
                {
                    this->createFormatter(output);
                    //go to process
                    res=this->isInitialized=true;
                }
                else
                {
                    std::cerr << "can't open output file!" << std::endl;
                    this->Finish();
                }
            }
        }
        else std::cerr << "bad cmdline params or bad input file!" << std::endl;
    }

    return res;
}

/*!
 * \brief AppFacade::Process - grab strings from the input and process it
 */
void AppFacade::Process()
{    
    if(this->isInitialized)
    {
        std::size_t dummy;
        MemStringSource::String str;
        while(1)
        {
            this->text->GetString(dummy,str);
            if(dummy==MemStringSource::LINE_EOF) break;
            this->processString(str);
        }
    }
}

/*!
 * \brief AppFacade::Finish - frees resources
 */
void AppFacade::Finish()
{    
    if(this->isInitialized)
    {
        this->output.close();
        this->text->Close();
        this->text.reset(nullptr);
        this->dictionary.reset(nullptr);
        this->formatter.reset(nullptr);
        this->isInitialized=false;
    }
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include "testhelpers.h"

TEST_SUITE("AppFacade tests")
{

    TEST_CASE("Positive")
    {
        const std::string file_path1 {"/tmp/input-spc1"};
        const std::string file_path2 {"/tmp/output-ref-spc1"};
        const std::string file_path3 {"/tmp/output-spc1"};

        REQUIRE(TestHelpers::make_test_file(file_path1));
        REQUIRE(TestHelpers::make_outref_file(file_path2));

        REQUIRE(AppFacade::Instance().Init(file_path1,file_path3));
        AppFacade::Instance().Process();

        CHECK(TestHelpers::compare_files(file_path2,file_path3));

        AppFacade::Instance().Finish();
    }
}
#endif
