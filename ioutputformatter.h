// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef IOUTPUTFORMATTER_H
#define IOUTPUTFORMATTER_H

#include <string>
#include <vector>

/*!
 * \brief The IOutputFormatter class - it's the interface to the output formatter implementaion
 */
class IOutputFormatter
{
private:
    IOutputFormatter(const IOutputFormatter&)=delete;
    IOutputFormatter& operator = (const IOutputFormatter&)=delete;
public:
    using String=std::string;
    using Strings=std::vector<String>;

    explicit IOutputFormatter() {}
    virtual ~IOutputFormatter() {}    

    virtual void AddCorrectWord(const String& w)=0;//print w
    virtual void AddCorrectedWord(const String& w)=0;//print w
    virtual void AddNotCorrectedWord(const String& w)=0;//print {w?}
    virtual void AddNotCorrectedWords(const Strings& variants)=0;//print {w1 w2...}
    virtual void AddNewLine(void)=0;
    virtual void AddSpacer(void)=0;
};

#endif // IOUTPUTFORMATTER_H
