// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "errouswordsgenerator.h"
#include "stringutils.h"

#if DEBUG
#include <iostream>
#endif

ErrousWordsGenerator::ErrousWordsGenerator(const std::string &src, const Strings &dict)
{
    this->values.clear();
    if(src.empty()||dict.empty()) return;

    std::size_t pos(0);

#if DEBUG==1
    std::cout << "------------------------" << std::endl;
    std::cout << "src: " << src << ", dict size: " << dict.size() << std::endl;
#endif

    for(String target:dict)
    {        
        std::size_t res=StringUtils::CalcEditDistance(src,target);

        if(res<=StringUtils::MAX_EDITS)
        {
            ErrousWord ew {pos,res,target};
            this->values.push_back(ew);
#if DEBUG==1
            std::cout << "target: " << ew.value << ", dist: " << ew.distance << std::endl;
#endif
        }

        pos++;
    }

#if DEBUG==1
    std::cout << "------------------------" << std::endl;
#endif

    //sort ewords by distance
    this->values.sort();
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include "testhelpers.h"

#include <iostream>

TEST_SUITE("ErrousWordsGenerator tests")
{
    TEST_CASE("Distance")
    {
        const std::string src {"hte"};
        const std::vector<std::string>& target=TestHelpers::get_dict_words_as_strings();

        ErrousWordsGenerator ewg(src,target);
        const ErrousWordsGenerator::ErrousWords &ew=ewg.GetResult();

        CHECK(ew.size()==1);
        CHECK(ew.cbegin()->distance==2);
        CHECK(ew.cbegin()->value==std::string("the"));
    }

    TEST_CASE("Sorting check")
    {
        const std::string src {"in"};
        const std::vector<std::string>& target=TestHelpers::get_dict_words_as_strings();

        ErrousWordsGenerator ewg(src,target);
        const ErrousWordsGenerator::ErrousWords &ew=ewg.GetResult();

        CHECK(ew.size()==5);
        CHECK(ew.cbegin()->distance==0);
        CHECK(ew.cbegin()->pos==8);
        CHECK(ew.cbegin()->value==std::string("in"));
        ErrousWordsGenerator::ErrousWords::const_reverse_iterator cri(ew.crend());
        cri++;
        CHECK(cri->distance==2);
        CHECK(cri->pos==9);
        CHECK(cri->value==std::string("on"));
    }
}
#endif
