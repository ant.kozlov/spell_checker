// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */

#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include <string>

/*!
 * \brief  StringUtils contains 'editorial distance' calculator and toLower utility
 */
namespace StringUtils
{
    const std::size_t MAX_EDITS {2};
    const std::size_t NO_VARIANTS {10};

    std::string toLowerStr(std::string s);
    std::size_t CalcEditDistance(const std::string& src,const std::string& target);
}

#endif // STRINGUTILS_H
