// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "simpledictionary.h"
#include "errouswordsgenerator.h"
#include "stringutils.h"

#include <sstream>
#include <algorithm>

bool SimpleDictionary::FindExact(const IDictionary::String &word_in_lower_case) const
{    
    if(word_in_lower_case.empty()||this->dict.empty()) return false;
    else return (std::find(this->dict.begin(),this->dict.end(),
                           word_in_lower_case)!=this->dict.end());
}

bool SimpleDictionary::FindSimilar(const IDictionary::String &word_in_lower_case,
                                   IDictionary::Strings &similars) const
{
    if(word_in_lower_case.empty()||this->dict.empty()) return false;
    similars.clear();

    ErrousWordsGenerator ewg(word_in_lower_case,this->dict);
    //result is sorted by the distance
    ErrousWordsGenerator::ErrousWords ews(ewg.GetResult());

    ///\brief implement all needed logic here for the case when given word is not in the dict
    if(ews.empty()) return false;///<no corrections was found

    ///\brief ignore any d==2 corrections if there is at least one d==1
    /// we have got sorted list in which elements with less distance placed at the start
    /// fill the output with an elements that has minimal distance
    std::size_t distance(ews.cbegin()->distance);
    for(ErrousWordsGenerator::ErrousWord e:ews)
    {
        if(e.distance==distance) similars.push_back(e.value);
    }

    return (!similars.empty());
}

void SimpleDictionary::AddWordsFromLine(const IDictionary::String &str)
{
    if(str.empty()) return;

    std::istringstream stream(str);

    std::string word;

    while(std::getline(stream,word,' ')) this->dict.push_back(StringUtils::toLowerStr(word));
}

void SimpleDictionary::Close()
{
    dict.clear();
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include "testhelpers.h"

#include <iostream>

TEST_SUITE("SimpleDictionary tests")
{
    const std::string IN_FILE_PATH {"/tmp/input-t1.txt"};
    const std::string DELIMITER {"==="};

    TEST_CASE("AddWordsFromLine & FindExact positive test")
    {
        SimpleDictionary d;
        d.AddWordsFromLine(TestHelpers::get_dict_words_as_string());

        //"rain spain plain plaint pain main mainly"
        //"the in on fall falls his was"
        CHECK(d.FindExact("rain"));
        CHECK(d.FindExact("spain"));
        CHECK(d.FindExact("in"));
        CHECK(d.FindExact("was"));
    }

    TEST_CASE("AddWordsFromLine & FindExact negative test")
    {
        SimpleDictionary d;
        d.AddWordsFromLine(TestHelpers::get_dict_words_as_string());

        //"rain spain plain plaint pain main mainly"
        //"the in on fall falls his was"
        CHECK_FALSE(d.FindExact("rainy"));
        CHECK_FALSE(d.FindExact("aspain"));
        CHECK_FALSE(d.FindExact("ine"));
        CHECK_FALSE(d.FindExact("wasy"));
    }

    TEST_CASE("FindSimilar positive test")
    {
        SimpleDictionary d;
        d.AddWordsFromLine(TestHelpers::get_dict_words_as_string());
        SimpleDictionary::Strings similars;

        //hte rame in pain fells
        //mainy oon teh lain
        //was hints pliant
        CHECK(d.FindSimilar("hte",similars));
        CHECK(similars.size()==1);
        CHECK(similars[0]==std::string("the"));

        CHECK_FALSE(d.FindSimilar("rame",similars));
        CHECK(similars.empty());

        CHECK(d.FindSimilar("fells",similars));
        CHECK(similars.size()==1);
        CHECK(similars[0]==std::string("falls"));

        CHECK(d.FindSimilar("mainy",similars));
        CHECK(similars.size()==2);
        CHECK(similars[0]==std::string("main"));
        CHECK(similars[1]==std::string("mainly"));

        CHECK(d.FindSimilar("oon",similars));
        CHECK(similars.size()==1);
        CHECK(similars[0]==std::string("on"));

        CHECK(d.FindSimilar("teh",similars));
        CHECK(similars.size()==1);
        CHECK(similars[0]==std::string("the"));

        CHECK(d.FindSimilar("lain",similars));
        CHECK(similars.size()==1);
        CHECK(similars[0]==std::string("plain"));

        CHECK_FALSE(d.FindSimilar("hints",similars));
        CHECK(similars.empty());

        CHECK(d.FindSimilar("pliant",similars));
        CHECK(similars.size()==1);
        CHECK(similars[0]==std::string("plaint"));
    }
}

#endif
