// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "testhelpers.h"

#if UNIT_TEST==1

#include <fstream>
#include <sstream>

namespace TestHelpers
{
////    Example
//    input:
//    rain spain plain plaint pain main mainly
//    the in on fall falls his was
//    ===
//    hte rame in pain fells
//    mainy oon teh lain
//    was hints pliant
//    ===

//    execute: spell_checker input output

//    output:
//            the {rame?} in pain falls
//            {main mainly} on the plain
//            was {hints?} plaint

const std::string input_text {"hte rame in pain fells\n"
                                  "mainy oon teh lain\n"
                                  "was hints pliant\n"};

const std::string& get_input_text(void)
{
    return input_text;
}

const std::string dict_words("rain spain plain plaint pain main mainly "
                       "the in on fall falls his was");

const std::string& get_dict_words_as_string(void)
{
    return dict_words;
}

static std::vector<std::string> dict_words_as_vector;

const std::vector<std::string>& get_dict_words_as_strings(void)
{
    if(dict_words_as_vector.empty())
    {
        std::istringstream stream(dict_words);

        std::string word;

        while(std::getline(stream,word,' ')) dict_words_as_vector.push_back(word);
    }

    return const_cast<const std::vector<std::string>&>(dict_words_as_vector);
}

bool make_test_file(const std::string& path)
{
    const std::string text("rain spain plain plaint pain main mainly\n"
                           "the in on fall falls his was\n"
                           "===\n"
                           "hte rame in pain fells\n"
                           "mainy oon teh lain\n"
                           "was hints pliant\n"
                           "===\n");

    std::ofstream ofile;
    bool res(false);

    ofile.open(path);

    if(ofile.is_open())
    {
        ofile << text;
        ofile.close();
        res=true;
    }

    return res;
}

bool make_outref_file(const std::string& path)
{
    const std::string text("the {rame?} in pain falls\n"
                           "{main mainly} on the plain\n"
                           "was {hints?} plaint\n");

    std::ofstream ofile;
    bool res(false);

    ofile.open(path);

    if(ofile.is_open())
    {
        ofile << text;
        ofile.close();
        res=true;
    }

    return res;
}

bool compare_files(const std::string& path1,const std::string& path2)
{
    std::ifstream file1,file2;
    bool res(false);

    file1.open(path1);
    file2.open(path2);

    if(file1.is_open()&&file2.is_open())
    {
        do
        {
            if(file1.get()!=file2.get()) break;
        }
        while(!file1.eof()||!file2.eof());

        res=file1.eof()&&file2.eof();
    }

    return res;
}

}
#endif
