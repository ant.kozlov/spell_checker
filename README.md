# spell_checker

It is an implementation of a spelling checker. It corrects words by finding words in the dictionary that are no more than two edits away from the input. It grabs the input file and puts output to the output file.

## Usage

```
$ spell_checker input-file output-file
```

The input file must contain the dictionary (words splitted by spaces) placed from the start of file until the delimiter ('===') and after that the text for checking ending by same delimiter. The output file contains corrected text.

## Installation
To build the spell_checker do this:
1. Make sure that you have GCC and CMake installed.
2. Run build.sh script.
3. After the build you will find spell_checker's binary in the 'build' directory.
