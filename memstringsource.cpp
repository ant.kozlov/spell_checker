// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "memstringsource.h"

#include <iostream>

bool MemStringSource::reopen(const IStringSource::String &resource,std::size_t offset)
{
    //if resource was opened already - close it
    if(this->infile.is_open()) this->Close();
    this->infile.open(resource);

    if(this->infile.is_open()) infile.seekg(offset);

    return this->infile.is_open();
}

bool MemStringSource::init_pool(void) const
{
    //it's faster than call size() in the cycle
    std::size_t counter(0);

    //fill poll with strings
    do
    {
        String line;
        std::getline(infile,line);
        //read strings until EOF or "===" reached
        if(this->infile.eof()||this->infile.fail()||line==this->delimiter) break;
        else this->pool.push(line);
    }
    while(counter++<POOL_SIZE);

    return !this->pool.empty();
}

void MemStringSource::clear_pool()
{
    while(!this->pool.empty()) this->pool.pop();
}

/*!
 * \brief MemStringSource::GetString - gets an string from the pool
 * \param lnumOut - line number
 * \param strOut - value
 */
void MemStringSource::GetString(std::size_t &lnumOut, IStringSource::String &strOut) const
{
    std::lock_guard<std::mutex> lock(this->pool_mutex);

    if(this->pool.empty()&&!this->init_pool())
    {
        //no more lines - send EOF
        lnumOut=LINE_EOF;
        return;
    }

    strOut=this->pool.front();this->pool.pop();

    lnumOut=this->linesCounter++;
}

/*!
 * \brief MemStringSource::Load - opens file and fill the pool with strings
 * \param resource
 * \return
 */
bool MemStringSource::Load(const IStringSource::String &resource,const String& delimiter,std::size_t offset)
{
    bool res(false);    
    this->delimiter=delimiter;

    if(this->reopen(resource,offset)&&this->init_pool())
    {
#if DEBUG==1
        std::cout << "The file '" << resource << "' was opened.\n";
#endif
        this->linesCounter=1;
        res=true;
    }
    else
    {
        std::cerr << "Can't open file '" << resource << "' or it's empty!\n";
        this->Close();
    }

    return res;
}

void MemStringSource::Close()
{
    //clear pool
    this->clear_pool();
    this->infile.close();

    this->linesCounter=LINE_EOF;
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include "testhelpers.h"

TEST_SUITE("MemStringSource tests")
{
    const std::string IN_FILE_PATH {"/tmp/input-t1.txt"};
    const std::string DELIMITER {"==="};

    TEST_CASE("Positive1")
    {
        MemStringSource mms;

        REQUIRE(TestHelpers::make_test_file(IN_FILE_PATH));
        REQUIRE(mms.Load(IN_FILE_PATH,DELIMITER,TestHelpers::TEXT_OFFSET_REF));

        std::string res;

        for(int counter=0;;counter++)
        {
            std::size_t num(0);
            MemStringSource::String str;
            mms.GetString(num,str);
            if(num==MemStringSource::LINE_EOF) break;
            res.append(str);
            res+='\n';
        }
        mms.Close();

        CHECK(res==TestHelpers::get_input_text());
    }
}
#endif
