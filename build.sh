#!/bin/bash

echo "Prepare for build..."

rm -R ./build

mkdir ./build

cd ./build

cmake ../

make

