// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef SIMPLEDICTIONARY_H
#define SIMPLEDICTIONARY_H

#include "idictionary.h"

#include <vector>
#include <string>

/*!
* \brief In the SimpleDictionary implementation we will use the simplest algo:
* \details at first - we'll try to find exact word in the dictionary
* \details  if there is no exact match we'll go to stage 2
* \details at stage 2 - we'll calculate Levenshtein (editor) distance between the target word and all words in the dictionary.
* \details After that we'll pass suitable variants for the further processing.
*/
class SimpleDictionary : public IDictionary
{
private:
    std::vector<std::string> dict;

public:
    explicit SimpleDictionary() {this->dict.clear();}
    ~SimpleDictionary() {this->Close();}

    /*!
     * \brief Find - seach the word in the dictionary
     * \param word - word to find
     * \return true if exact match was finded
     */
    bool FindExact(const String& word_in_lower_case) const final;
    /*!
     * \brief FindSimilar - seach similar words in the dictionary
     * \param word - word to find
     * \param similars - a list with similar words in the most matching order
     * \return true if any variants was founded
     */
    bool FindSimilar(const String& word_in_lower_case,Strings& similars) const final;
    /*!
     * \brief AddWordsFromLine - it fills the dictionary with words from the string
     */
    void AddWordsFromLine(const String& str) final;
    /*!
     * \brief Free resources
     */
    void Close(void) final;
};

#endif // SIMPLEDICTIONARY_H
