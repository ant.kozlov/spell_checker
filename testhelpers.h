// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef TESTHELPERS_H
#define TESTHELPERS_H

#if UNIT_TEST==1

#include <string>
#include <vector>

namespace TestHelpers
{
    const std::size_t TEXT_OFFSET_REF {0x4A};

    const std::string& get_input_text(void);
    const std::string& get_dict_words_as_string(void);
    const std::vector<std::string>& get_dict_words_as_strings(void);
    bool make_test_file(const std::string& path);
    bool make_outref_file(const std::string& path);
    bool compare_files(const std::string& path1,const std::string& path2);
}

#endif

#endif // TESTHELPERS_H
