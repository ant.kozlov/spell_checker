// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include <iostream>

#include "appfacade.h"

#if UNIT_TEST==0
int main(int argc,char* argv[])
{    
    std::cout << "Spell_checker v1.0.0";

    if(argc==3)
    {
        std::cout << std::endl;

        std::string in_filename(argv[1]);
        std::string out_filename(argv[2]);

        if(AppFacade::Instance().Init(in_filename,out_filename))
        {
            AppFacade::Instance().Process();
            AppFacade::Instance().Finish();
        }
    }
    else std::cout << ", usage: spell_checker in_filename out_filename\n";

    return 0;
}
#else
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "test/doctest.h"
#endif

