// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef SIMPLEFORMATTER_H
#define SIMPLEFORMATTER_H

#include "ioutputformatter.h"

#include <iostream>

/*!
 * \brief The SimpleFormatter class - implements output generation
 */
class SimpleFormatter : public IOutputFormatter
{
private:
    std::ostream& out;
public:
    explicit SimpleFormatter(void):out(std::cout) {}
    explicit SimpleFormatter(std::ostream& o):out(o) {}

    inline void AddCorrectWord(const String& w) {if(!w.empty()) out << w;}//print w
    inline void AddCorrectedWord(const String& w) {this->AddCorrectWord(w);}//print w
    inline void AddNotCorrectedWord(const String& w) {if(!w.empty()) out << '{' << w <<"?}";}//print {w?}
    void AddNotCorrectedWords(const Strings& variants);//print {w1 w2...}
    inline void AddNewLine(void) {out << std::endl;}
    inline void AddSpacer(void) {out << " ";}
};

#endif // SIMPLEFORMATTER_H
