// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */

#ifndef APPFACADE_H
#define APPFACADE_H

#include <fstream>
#include <string>
#include <memory>

#include "istringsource.h"
#include "ioutputformatter.h"
#include "idictionary.h"

/*!
 * \brief The AppFacade class contains all business logic of the app
 */
class AppFacade
{
private:
    using IStringSourcePtr=std::unique_ptr<IStringSource>;
    using IOutputFormatterPtr=std::unique_ptr<IOutputFormatter>;    
    using IDictionaryPtr=std::unique_ptr<IDictionary>;

    const std::string DELIMITER {"==="};

    bool isInitialized;

    IStringSourcePtr text;
    IOutputFormatterPtr formatter;
    IDictionaryPtr dictionary;

    std::ofstream output;

    AppFacade():isInitialized(false) {}

    bool validateFilename(const std::string& fn) const;
    bool processInputFile(const std::string& fn, std::size_t& text_offset, IDictionary &dict) const;
    void processString(const std::string& s) const;

    void createStringSource(void);
    void createFormatter(std::ostream& output);
    void createDictionary(void);

public:
    static AppFacade& Instance()
    {
        static AppFacade instance;
        return instance;
    }

    bool Init(const std::string& in_fname,const std::string& out_fname);
    void Process(void);
    void Finish(void);

    AppFacade(const AppFacade&) = delete;
    AppFacade& operator=(const AppFacade&)  = delete;
};

#endif // APPFACADE_H
