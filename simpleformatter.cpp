// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "simpleformatter.h"

void SimpleFormatter::AddNotCorrectedWords(const IOutputFormatter::Strings &variants)
{
    if(variants.empty()) return;

    String str("{");

    for(const String& s : variants)
    {
        str+=s;
        str+=' ';
    }

    str.erase(--str.cend());

    str+='}';

    this->out << str;
}

#if UNIT_TEST==1
#include "test/doctest.h"

#include <sstream>

TEST_SUITE("SimpleFormatter tests")
{
    const std::string reference {"test test {test?} {test test test}\ntest test {test?} {test test test}"};

    TEST_CASE("Positive")
    {        
        std::ostringstream buf;
        const std::string test_word {"test"};
        const std::vector<std::string> test_words {test_word,test_word,test_word};

        SimpleFormatter formatter(static_cast<std::ostream&>(buf));

        formatter.AddCorrectWord(test_word);
        formatter.AddSpacer();
        formatter.AddCorrectedWord(test_word);
        formatter.AddSpacer();
        formatter.AddNotCorrectedWord(test_word);
        formatter.AddSpacer();
        formatter.AddNotCorrectedWords(test_words);
        formatter.AddNewLine();
        formatter.AddCorrectWord(test_word);
        formatter.AddSpacer();
        formatter.AddCorrectedWord(test_word);
        formatter.AddSpacer();
        formatter.AddNotCorrectedWord(test_word);
        formatter.AddSpacer();
        formatter.AddNotCorrectedWords(test_words);

#if DEBUG==1
        std::cout << buf.str() << std::endl;
#endif

        CHECK(reference==buf.str());
    }
}
#endif
