// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#ifndef IDICTIONARY_H
#define IDICTIONARY_H

#include <string>
#include <vector>
#include <memory>

/*!
 * \brief The IDictionary class - it's the interface to the dictionary
 */
class IDictionary
{
private:
    IDictionary(const IDictionary&)=delete;
    IDictionary& operator = (const IDictionary&)=delete;
protected:

public:
    explicit IDictionary() {}
    virtual ~IDictionary() {}

    using String=std::string;
    using Strings=std::vector<String>;
    using IDictionaryPtr=std::unique_ptr<IDictionary>;

    /*!
     * \brief Find - seach the word in the dictionary
     * \param word - word to find
     * \return true if exact match was founded
     */
    virtual bool FindExact(const String& word) const=0;
    /*!
     * \brief FindSimilar - seach similar words in the dictionary
     * \param word - word to find
     * \param similars - a list with similar words in the most matching order
     * \return true if any variants was founded
     */
    virtual bool FindSimilar(const String& word,Strings& similars) const=0;
    /*!
     * \brief AddWordsFromLine - it fills the dictionary with words from the string
     */
    virtual void AddWordsFromLine(const String& str)=0;
    /*!
     * \brief Free resources
     */
    virtual void Close(void)=0;
};

#endif // IDICTIONARY_H
