// SPDX-License-Identifier: GPL-3.0-only
/*
 * Copyright (C) 2020 Anton Kozlov
 * Released under GNU GPL v3, read the file 'LICENSE' for more information.
 */
#include "stringutils.h"

#include <algorithm>
#include <vector>

#if DEBUG==1
#include <iostream>
#endif

namespace StringUtils {

std::string toLowerStr(std::string s)
{
    std::transform(s.begin(), s.end(), s.begin(),
                   [](unsigned char c){ return std::tolower(c); }
    );
    return s;
}

///\brief This variant fails test on dual adjacent ops but it's faster
#if FAST_BUT_DIRTY==1
/*!
 *\brief GeneralizedLevenshteinDistance is based on implementation from wikibooks.org
 * \link https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C++
 */
template<typename T>
static
typename T::size_type GeneralizedLevenshteinDistance(const T &source,
                                                     const T &target,
                                                     typename T::size_type insert_cost = 1,
                                                     typename T::size_type delete_cost = 1,
                                                     typename T::size_type replace_cost = 1)
{
    if (source.size() > target.size())
    {
        return GeneralizedLevenshteinDistance(target, source, delete_cost, insert_cost, replace_cost);
    }

    using TSizeType = typename T::size_type;
    const TSizeType min_size = source.size(), max_size = target.size();
    std::vector<TSizeType> lev_dist(min_size + 1);

    lev_dist[0] = 0;
    for (TSizeType i = 1; i <= min_size; ++i)
    {
        lev_dist[i] = lev_dist[i - 1] + delete_cost;
    }

    for (TSizeType j = 1; j <= max_size; ++j)
    {
        TSizeType previous_diagonal = lev_dist[0], previous_diagonal_save;
        lev_dist[0] += insert_cost;

        for (TSizeType i = 1; i <= min_size; ++i)
        {
            previous_diagonal_save = lev_dist[i];

            if (source[i - 1] == target[j - 1])
            {
                lev_dist[i] = previous_diagonal;
            }
            else
            {
                std::size_t del=lev_dist[i - 1] + delete_cost;
                std::size_t ins=lev_dist[i] + insert_cost;

                lev_dist[i] = std::min(std::min(del, ins), previous_diagonal + replace_cost);
            }
            previous_diagonal = previous_diagonal_save;
        }
    }

    return lev_dist[min_size];
}

#else

#if DEBUG==1
template <typename DistanceType, typename IndexType>
static void printMatrix(const std::vector<std::vector<DistanceType>>& matrix)
{
    if(matrix.empty()) return;
    const IndexType len1 = matrix.size(), len2 = matrix[0].size();

    std::cout << "matrix:\n";

    for(IndexType i = 0; i < len1; ++i)
    {
        for(IndexType j = 0; j < len2; ++j)
        {
            DistanceType value(matrix[i][j]&(DistanceType)0x0F);
            DistanceType mark(matrix[i][j]&(DistanceType)0x80);

            if(mark) std::cout << "*" << value << " ";
            else std::cout << " " << value << " ";
        }
        std::cout <<  "\n";
    }
}
#endif

/*!
 *\brief edit_distance is based on implementation from wikibooks.org
 * \link https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C++
 */
template <typename DistanceType, typename IndexType>
static unsigned int edit_distance(const std::string& s1, const std::string& s2)
{
    const IndexType len1 = (IndexType)s1.size(), len2 = (IndexType)s2.size();
    std::vector<std::vector<DistanceType>> d(len1 + 1, std::vector<DistanceType>(len2 + 1));

    ///init matrix
    d[0][0] = 0;
    for(IndexType i = 1; i <= len1; ++i) d[i][0] = i;
    for(IndexType i = 1; i <= len2; ++i) d[0][i] = i;

    //calc distances
    for(IndexType i = 1; i <= len1; ++i)
        for(IndexType j = 1; j <= len2; ++j)
            d[i][j] = std::min({ d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + (s1[i - 1] == s2[j - 1] ? 0 : 10) });

    DistanceType minimalDistance(d[len1][len2]);

    if(minimalDistance<=(DistanceType)MAX_EDITS)
    {
        //check for dual adjacent ops
        IndexType i(len1),j(len2);
#if DEBUG==1
        std::string path;
#endif
        DistanceType lastOp(0);

        while(i>0&&j>0)
        {
            DistanceType del=d[i-1][j]+1;
            DistanceType ins=d[i][j-1]+1;
            DistanceType cost=(s1[i-1]==s2[j-1])?0:10;
            DistanceType mr=d[i-1][j-1]+cost;
#if DEBUG==1
            d[i][j]|=(DistanceType)0x80;
#endif
            if(del<ins)
            {
                if(del<mr)
                {
                    i--;
#if DEBUG==1
                    path+='D';
#endif
                    if(lastOp==(DistanceType)'D') {minimalDistance=(DistanceType)NO_VARIANTS;break;}
                    else lastOp=(DistanceType)'D';
                }
                else
                {
                    i--;j--;
#if DEBUG==1
                    path+='M';
#endif
                    lastOp=(DistanceType)'M';
                }
            }
            else
            {
                if(ins<mr)
                {
                    j--;
#if DEBUG==1
                    path+='I';
#endif
                    if(lastOp==(DistanceType)'I') {minimalDistance=(DistanceType)NO_VARIANTS;break;}
                    else lastOp=(DistanceType)'I';
                }
                else
                {
                    i--;j--;
#if DEBUG==1
                    path+='M';
#endif
                    lastOp=(DistanceType)'M';
                }
            }
        }

#if DEBUG==1
        std::cout << "edit_distance: " << "s1=" << s1 << ", s2=" << s2 << "\n";
        printMatrix<DistanceType,IndexType>(d);
        std::cout << "path=" << path << ", isBad=" << (minimalDistance==NO_VARIANTS) << "\n";
#endif
    }

    return minimalDistance;
}
#endif

std::size_t CalcEditDistance(const std::string& src,const std::string& target)
{
#if FAST_BUT_DIRTY==1
    return GeneralizedLevenshteinDistance<std::string>(src,target,1,1,10);
#else
    return edit_distance<std::uint_fast32_t,std::uint_fast32_t>(src,target);
#endif
}

}

#if UNIT_TEST==1
#include "test/doctest.h"

TEST_CASE("toLowerStr test")
{
    const std::string test {"Abra CadaBra"};
    const std::string reference {"abra cadabra"};

    CHECK(StringUtils::toLowerStr(test)==reference);
}

TEST_SUITE("Editor distance tests")
{
    TEST_CASE("Test")
    {
        CHECK(StringUtils::edit_distance<std::size_t,std::size_t>("hints","his")==StringUtils::NO_VARIANTS);
        CHECK(StringUtils::edit_distance<std::size_t,std::size_t>("hints","hions")!=StringUtils::NO_VARIANTS);
        CHECK(StringUtils::edit_distance<std::size_t,std::size_t>("hints","hinoits")==StringUtils::NO_VARIANTS);
    }
}

#endif
